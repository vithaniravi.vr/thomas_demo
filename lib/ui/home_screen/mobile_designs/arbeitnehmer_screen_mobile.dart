import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../constants/color_constants.dart';

class ArbeithnehmerScreenMobile extends StatelessWidget {
  const ArbeithnehmerScreenMobile({super.key});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      primary: false,
      physics: const NeverScrollableScrollPhysics(),
      child: Column(
        children: [
          Container(
            width: double.infinity,
            color: Colors.white,
            child: Text(
              'Drei einfache Schritte\nzu deinem neuen Job',
              textAlign: TextAlign.center,
              style: GoogleFonts.lato(
                color: ColorConstants.getInstance().textColor,
                textStyle: Theme.of(context).textTheme.headlineSmall,
                fontWeight: FontWeight.normal,
                letterSpacing: 1,
              ),
            ),
          ),
          Container(
            height: 30,
            color: Colors.white,
          ),
          Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [ColorConstants.getInstance().gradientColor1, ColorConstants.getInstance().gradientColor2],
              ),
            ),
            child: ClipPath(
              clipper: WaveClipper(),
              child: Container(
                width: double.infinity,
                color: Colors.white,
                height: MediaQuery.of(context).size.height / 2.5,
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Positioned(
                      top: 30,
                      right: 1,
                      child: SvgPicture.asset(
                        'assets/vectors/arbeitnehmer1.svg',
                        height: MediaQuery.of(context).size.height / 5.5,
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 18.0),
                        child: RichText(
                          maxLines: 2,
                          text: TextSpan(
                            text: '1. ',
                            style: GoogleFonts.lato(
                              color: ColorConstants.getInstance().contentTextColor,
                              textStyle: Theme.of(context).textTheme.displayLarge,
                              fontWeight: FontWeight.normal,
                              // letterSpacing: 1,
                            ),
                            children: [
                              TextSpan(
                                text: 'Erstellen dein Lebenslauf',
                                style: GoogleFonts.lato(
                                  color: ColorConstants.getInstance().contentTextColor,
                                  textStyle: Theme.of(context).textTheme.titleLarge,
                                  fontWeight: FontWeight.normal,
                                  // letterSpacing: 1,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          ClipPath(
            clipper: WaveClipper(),
            child: Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height / 2,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [ColorConstants.getInstance().gradientColor2, ColorConstants.getInstance().gradientColor1],
                ),
              ),
              padding: const EdgeInsets.symmetric(horizontal: 30),
              child: Stack(
                children: [
                  Align(
                    alignment: Alignment.centerRight,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 50.0, bottom: 10),
                      child: SvgPicture.asset(
                        'assets/vectors/arbeitnehmer2.svg',
                        height: MediaQuery.of(context).size.height / 5.5,
                      ),
                    ),
                  ),
                  Positioned(
                    top: 1,
                    left: 1,
                    child: RichText(
                      text: TextSpan(
                        text: '2. ',
                        style: GoogleFonts.lato(
                          color: ColorConstants.getInstance().contentTextColor,
                          textStyle: Theme.of(context).textTheme.displayLarge,
                          fontWeight: FontWeight.normal,
                          // letterSpacing: 1,
                        ),
                        children: [
                          TextSpan(
                            text: 'Erstellen dein Lebenslauf',
                            style: GoogleFonts.lato(
                              color: ColorConstants.getInstance().contentTextColor,
                              textStyle: Theme.of(context).textTheme.titleLarge,
                              fontWeight: FontWeight.normal,
                              // letterSpacing: 1,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            width: double.infinity,
            height: MediaQuery.of(context).size.height / 3,
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Stack(
              children: [
                Positioned(
                  bottom: 1,
                  right: 1,
                  child: SvgPicture.asset(
                    'assets/vectors/arbeitnehmer3.svg',
                    height: MediaQuery.of(context).size.height / 5.5,
                  ),
                ),
                Row(
                  children: [
                    Text(
                      '3.',
                      style: GoogleFonts.lato(
                        color: ColorConstants.getInstance().contentTextColor,
                        textStyle: Theme.of(context).textTheme.displayLarge,
                        fontWeight: FontWeight.normal,
                        // letterSpacing: 1,
                      ),
                    ),
                    Flexible(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20.0, top: 20),
                        child: Text(
                          'Mit nur einem Klick\nbewerben',
                          style: GoogleFonts.lato(
                            color: ColorConstants.getInstance().contentTextColor,
                            textStyle: Theme.of(context).textTheme.titleLarge,
                            fontWeight: FontWeight.normal,
                            // letterSpacing: 1,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class WaveClip extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = new Path();
    final lowPoint = size.height - 30;
    final highPoint = size.height - 60;
    path.lineTo(0, size.height);
    path.quadraticBezierTo(size.width / 4, highPoint, size.width / 2, lowPoint);
    path.quadraticBezierTo(3 / 4 * size.width, size.height, size.width, lowPoint);
    path.lineTo(size.width, 0);
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}

class WaveClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = new Path();
    path.lineTo(0, size.height); //start path with this if you are making at bottom

    var firstStart = Offset(size.width / 5, size.height);
    //fist point of quadratic bezier curve
    var firstEnd = Offset(size.width / 2.25, size.height - 50.0);
    //second point of quadratic bezier curve
    path.quadraticBezierTo(firstStart.dx, firstStart.dy, firstEnd.dx, firstEnd.dy);

    var secondStart = Offset(size.width - (size.width / 3.24), size.height - 105);
    //third point of quadratic bezier curve
    var secondEnd = Offset(size.width, size.height - 10);
    //fourth point of quadratic bezier curve
    path.quadraticBezierTo(secondStart.dx, secondStart.dy, secondEnd.dx, secondEnd.dy);

    path.lineTo(size.width, 0); //end with this path if you are making wave at bottom
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false; //if new instance have different instance than old instance
    //then you must return true;
  }
}

class BottomWaveClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = new Path();

    path.lineTo(0.0, size.height - 40);

    path.quadraticBezierTo(size.width / 4, size.height - 80, size.width / 2, size.height - 40);

    path.quadraticBezierTo(size.width - (size.width / 4), size.height, size.width, size.height - 40);

    path.lineTo(size.width, 0.0);

    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}
